#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "0.2.1"

import random


class RandomDeck:
    """This class generates random deck"""

    def __init__(self, cards: int, cards_set: []):
        """Ctor
        :param cards: Amount of cards to generate
        :return: Deck filled with random cards"""
        self.cards = list()
        if cards > len(cards_set): cards = len(cards_set)
        while cards >= 1:
            card_id = random.randrange(0, len(cards_set))
            card = cards_set[card_id]
            self.cards.append(card)
            cards_set.pop(card_id)
            cards -= 1

    def __repr__(self) -> []:
        """This method returns random deck in form of a list"""
        return self.cards

    def __str__(self) -> str:
        """This method returns generated deck in form of a string"""
        printable_set = list()
        size = len(self.cards)
        for card in range(size):
            printable_set.append(str(self.cards[card]))
        return f"Random deck of size {size}:\n"+"\n".join(printable_set)
