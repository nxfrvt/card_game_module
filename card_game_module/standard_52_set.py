#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "0.2.2"

import json
import pathlib
from card_game_module.card import Card


class Standard52Set:
    """Class of 52 French playing cards set"""

    def __init__(self):
        """Ctor
        When initialized generates
        the standard 52 cards playing deck"""
        path = str(pathlib.Path(__file__).parent.absolute())
        self.suits = ["Clubs", "Diamonds", "Hearts", "Spades"]
        self.cards_set = list()
        with open(path + '/cards.json') as json_file:
            data = json.load(json_file)
            for suit in self.suits:
                for c in data['cards']:
                    name = c['name'] + " of " + suit
                    temp_card = Card(name, c['value'])
                    self.cards_set.append(temp_card)

    def __repr__(self) -> []:
        """This method returns classic 52 playing cards set in form of a list"""
        return self.cards_set

    def __str__(self) -> str:
        """
        This method prints all the cards within the set
        with their values in form of a string
        """
        printable_set = list()
        for card in range(len(self.cards_set)):
            printable_set.append(str(self.cards_set[card]))
        return "\n".join(printable_set)
