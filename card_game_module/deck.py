#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "0.2.1"

import random
from card_game_module.card import Card


class Deck:
    """This is deck handling class"""

    def __init__(self, deck: []):
        """Ctor
        :param deck: Deck in form of a list
        """
        self.cards: [] = deck

    def draw_a_card(self) -> Card:
        """
        This method draws a single random card from a deck
        :return: Single random card
        """
        if len(self.cards) <= 0:
            pass
        else:
            drawn_card_id: int = random.randrange(0, len(self.cards))
            drawn_card: Card = self.cards[drawn_card_id]
            self.cards.pop(drawn_card_id)
            return drawn_card

    def draw_starting_hand(self, hand: int) -> []:
        """
        This method allows to draw starting hand
        from provided deck with specified amount
        of cards in starting hand
        :param hand: Amount of cards that should be drawn into a starting hand
        :return: List of drawn cards into the starting hand
        """
        starting_hand = list()
        while hand >= 1 and len(self.cards) > 0:
            starting_hand.append(self.draw_a_card())
            hand -= 1
        return starting_hand

    def __str__(self) -> str:
        """This method returns current state of deck in form of a string"""
        self.printable_set = list()
        size = len(self.cards)
        for card in range(size):
            self.printable_set.append(str(self.cards[card]))
        return "Deck:\n"+"\n".join(self.printable_set)

    def __repr__(self) -> []:
        """This method returns deck in from of a list"""
        return self.cards


class OwnedDeck(Deck):
    """This is deck with an owner class"""

    def __init__(self, deck: Deck, player: str):
        """
        Ctor
        :param deck: Deck we are giving an owner
        :param player: Owner of the deck
        """
        super(OwnedDeck, self).__init__(deck.cards)
        self.player: str = player

    def __str__(self) -> str:
        """This method returns current state of deck in form of a string"""
        super(OwnedDeck, self).__str__()
        return f"{self.player}'s deck:\n"+"\n".join(self.printable_set)
