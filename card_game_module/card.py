#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "0.2.1"


class Card:
    """this is Card class"""
    def __init__(self, name: str, value: int):
        """Ctor
        :param: name: Name of the card
        :param: value: Value of the card
        """
        self._name = name
        self._value = value

    @property
    def name(self):
        return self._name

    @property
    def value(self):
        return self._value

    def __str__(self):
        """
        This method prints the name and
        the value of the card
        """
        return f"{self.name}| Card value: {self.value}"

    def __gt__(self, other):
        """This method checks if card has greater value than the other"""
        return self.value > other.value

    def __eq__(self, other):
        """This method checks if cards are of the same value"""
        return self.value == other.value

    def __lt__(self, other):
        """This method checks if card has lesser value than the other"""
        return self.value < other.value
