#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "0.2.1"

from random import shuffle
from card_game_module.standard_52_set import Standard52Set
from card_game_module.random_deck import RandomDeck
from card_game_module.deck import Deck
from card_game_module.deck import OwnedDeck
from card_game_module.card import Card


def take_a_card(winner: OwnedDeck, looser: OwnedDeck):
    """
    This function handles after-round
    pass of the cards between the players
    """
    winner = winner.cards
    looser = looser.cards
    winner.append(looser[0])
    looser.pop(0)
    winner.append(winner[0])
    winner.pop(0)


class WarGame:
    """This class handles simulation of
    War card game"""

    def __init__(self, player_1: str, player_2: str):
        """Ctor
        :param: player_1: Name of the first player
        :param: player_2: Name of the second player"""
        self.player_1 = player_1
        self.player_2 = player_2
        self.__decks_setup()
        self.round_counter: int = 0
        while (len(self.deck_player_1.cards)) > 0 and (len(self.deck_player_2.cards)) > 0:
            self.__play()
            self.round_counter += 1
        if (len(self.deck_player_1.cards)) <= 0:
            print("{} won!".format(self.player_2))
            print("Game took {} rounds to resolve".format(self.round_counter))
        elif (len(self.deck_player_2.cards)) <= 0:
            print("{} won!".format(self.player_1))
            print("Game took {} rounds to resolve".format(self.round_counter))

    def __decks_setup(self):
        """This method generates two random decks for
        each of the players from provided cards set"""
        cards_set = Standard52Set().__repr__()
        temp_deck = Deck(RandomDeck(26, cards_set).cards)
        self.deck_player_1 = OwnedDeck(temp_deck, self.player_1)
        temp_deck = Deck(RandomDeck(26, cards_set).cards)
        self.deck_player_2 = OwnedDeck(temp_deck, self.player_2)

    def __play(self):
        """This method handles the "turn" mechanic,
        each player plays a card and the value is checked.
        With the result of the battle, proper event occurs"""
        if self.deck_player_1.cards[0] == self.deck_player_2.cards[0]:
            self.__war()
        elif self.deck_player_1.cards[0] > self.deck_player_2.cards[0]:
            take_a_card(self.deck_player_1, self.deck_player_2)
        else:
            take_a_card(self.deck_player_2, self.deck_player_1)

    def __war(self):
        """This method handles the war event
        which occurs when two players play cards
        of the same value. Three cards from each player
        are drawn and based of the values of the last ones,
        winner player takes all of the six cards drawn.
        If one of the players have less than 3 cards,
        he looses.
        In case of another draw, cards are given back
        to the players and decks are shuffled"""
        side_1 = self.deck_player_1
        side_2 = self.deck_player_2
        if len(side_1.cards) <= 2:
            side_1.cards.clear()
        elif len(side_2.cards) <= 2:
            side_2.cards.clear()
        else:
            card_pile = list()
            for num in range(3):
                card_pile.append(side_1.cards[num])
                card_pile.append(side_2.cards[num])
            war_card_1: Card = card_pile[len(card_pile) - 2]
            war_card_2: Card = card_pile[len(card_pile) - 1]
            if war_card_1 > war_card_2:
                for card in range(0, len(card_pile), 2):
                    take_a_card(side_1, side_2)
            elif war_card_2 > war_card_1:
                for card in range(0, len(card_pile), 2):
                    take_a_card(side_2, side_1)
            else:
                card_pile.clear()
                shuffle(side_1.cards)
                shuffle(side_2.cards)
