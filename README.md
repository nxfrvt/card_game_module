# Card game module [![pipeline status][pipeline]][master] [![coverage report][coverage]][master]

## Table of contents
* [General info](#general-info)
* [Authors](#authors)
* [License](#license)

## General info

Module for card games

## Authors
* Bartłomiej Mazurek


See also the list of [contributors][contributors] who participated in this project.

## License
[MIT](https://choosealicense.com/licenses/mit/)

[contributors]: https://gitlab.com/nxfrvt/card_game_module/-/graphs/master
[logo]: https://avatars0.githubusercontent.com/u/64803053?s=460&u=2009c05a55d5d25adb8264f8f6c419daed7e3bdb&v=4
[pipeline]: https://gitlab.com/nxfrvt/card_game_module/badges/master/pipeline.svg
[coverage]: https://gitlab.com/nxfrvt/card_game_module/badges/master/coverage.svg
[master]: https://gitlab.com/nxfrvt/card_game_module/-/commits/master
