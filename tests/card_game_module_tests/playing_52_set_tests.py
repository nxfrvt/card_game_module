#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "0.2.1"

import unittest
from card_game_module.standard_52_set import Standard52Set


class Standard52SetTest(unittest.TestCase):
    def set_check(self):
        french_set = Standard52Set.__repr__()
        self.assertEqual(52, len(french_set), "Generated set of cards is of"
                                              "wrong size of {}".format(len(french_set)))


if __name__ == "__main__":
    unittest.main()
