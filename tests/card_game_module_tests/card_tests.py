#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "0.2.1"

import unittest
from card_game_module.card import Card


class CardTest(unittest.TestCase):
    def test_ace(self):
        ace = Card("Ace", 14)
        self.assertEqual("Ace", ace.name, "Name {} does not match "
                                          "with expected {} name".format(ace.name, "Ace"))
        self.assertEqual(14, ace.value, "Expected value of {}, "
                                        "{} found instead".format(14, ace.value))

    def test_king(self):
        king = Card("King", 13)
        self.assertEqual("King", king.name, "Name {} does not match "
                                            "with expected {} name".format(king.name, "Ace"))
        self.assertEqual(13, king.value, "Expected value of {}, "
                                        "{} found instead".format(14, king.value))


if __name__ == "__main__":
    unittest.main()
