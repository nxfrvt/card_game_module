#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "0.2.1"

import unittest
from card_game_module.random_deck import RandomDeck
from card_game_module.standard_52_set import Standard52Set


class RandomDeckTest(unittest.TestCase):
    def test_15_cards(self):
        self.cards_set = Standard52Set().__repr__()
        deck_15 = RandomDeck(15, self.cards_set)
        self.assertEqual(15, len(deck_15.cards), "Generated deck is not of right size")

    def test_60_cards(self):
        self.cards_set = Standard52Set().__repr__()
        deck_60 = RandomDeck(60, self.cards_set)
        self.assertEqual(52, len(deck_60.cards), "Generated deck is of size {} instead"
                                                 "of max size of 52".format(len(deck_60.cards)))

    def test_n_cards(self):
        self.cards_set = Standard52Set().__repr__()
        deck_n = RandomDeck(-15, self.cards_set)
        self.assertEqual(0, len(deck_n.cards), "Generated deck is of size {} instead"
                                               "of min size of 0".format(len(deck_n.cards)))


if __name__ == "__main__":
    unittest.main()
