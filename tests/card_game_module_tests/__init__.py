#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "0.1"

import unittest
from tests.card_game_module_tests.deck_tests import DeckTest
from tests.card_game_module_tests.playing_52_set_tests import Standard52SetTest
from tests.card_game_module_tests.random_deck_tests import RandomDeckTest

if __name__ == "__main__":
    unittest.main()