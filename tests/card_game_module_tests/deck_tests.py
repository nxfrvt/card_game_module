#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "0.2.1"

import unittest
from card_game_module.deck import Deck
from card_game_module.random_deck import RandomDeck
from card_game_module.standard_52_set import Standard52Set


class DeckTest(unittest.TestCase):
    def test_draw_4_cards(self):
        self.deck = Deck(RandomDeck(15, Standard52Set().__repr__()).cards)
        self.hand = self.deck.draw_starting_hand(4)
        self.assertEqual(4, len(self.hand), "Amount of drawn cards"
                                            "is not of proper value {},"
                                            "expected {}".format(4, len(self.hand)))

    def test_size_of_drawn_deck(self):
        self.deck = Deck(RandomDeck(52, Standard52Set().__repr__()).cards)
        self.deck.draw_a_card()
        self.assertEqual(51, len(self.deck.cards), "Wrong size of deck of {},"
                                                   "{} expected".format(len(self.deck.cards), 51))

    def test_size_of_drawn_deck_at_0(self):
        self.deck = Deck((RandomDeck(1, Standard52Set().__repr__())).cards)
        self.deck.draw_a_card()
        self.assertEqual(0, len(self.deck.cards), "Wrong size of deck of {},"
                                                  "{} expected".format(0, len(self.deck.cards)))
        self.deck.draw_a_card()
        self.assertEqual(0, len(self.deck.cards), "Wrong size of deck of {},"
                                                  "{} expected".format(0, len(self.deck.cards)))

    def test_draw_full_deck(self):
        self.deck = Deck((RandomDeck(52, Standard52Set().__repr__())).cards)
        self.hand = self.deck.draw_starting_hand(54)
        self.assertEqual(0, len(self.deck.cards), "Wrong size of deck of {},"
                                                  "{} expected".format(0, len(self.deck.cards)))


if __name__ == "__main__":
    unittest.main()
